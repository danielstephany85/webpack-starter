const path = require('path');

module.exports = {
    entry: {
        app: './src/js/index.js'
    },
    output: {
        filename: "js/[name].bundle.js",
        path: path.resolve(__dirname, '../dist')
    }
}