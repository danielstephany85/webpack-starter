const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
    new HtmlWebpackPlugin({
        title: 'page 1',
        filename: 'index.html',
        template: './src/views/index.html',
        chunks: ['app']
    }),
    new HtmlWebpackPlugin({
        title: 'page 2',
        filename: 'test.html',
        template: './src/views/test.html',
        chunks: ['app']
    })
]